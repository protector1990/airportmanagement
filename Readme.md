Daon coding challenge, Marko Tanic entry

1. Running the app
In order to run this app, you have to have JDK 11 installed on your system. Since this app uses 
PostgreSQL as the database layer, it is necessary to provide it. For a simple way to provide 
Postgres db, you can pull this image from DockerHub:

docker pull protector1990/airportmanagement-postgres:final

and run it with

docker run --rm -p 5432:5432 <image_id>

Then you can run the application from either intellij idea, or pre built jar itlsef 
(java -jar build/airportmanagement-0.0.1-SNAPSHOT.jar)
Application runs on port http 8080, so it has to be free in order for the app to start

If setting up postgres manually, you have to run postgres-docker/init.sql in your database

2. REST API
You can find API calls packed inside a postman collection (postman/AirportManagement.postman_collection.json).
First of all, you should log in with Login request (POST /api/login). When app is first run, 
it creates a single admin user, with (admin, admin) username and password. When a new admin user is 
created through the API,this default admin gets deleted.

In order to proceed with further API requests, you'll have to extract JWT token and XSRF token
from cookies. JWT token should be returned as cookie, and XSRF token will have to be sent in
X-XSRF-TOKEN header. If postman won't create cookie header automatically, you'll have to 
manually create cookie header like this:

Cookie:X-JWT-COOKIE=e0u1YP3sWjHDStFbteRSqJSI+ufNc3k9p7n8g5ljHPxYPEoNQUplCw2ID98wNdwMML5jXTOjnXRZxlzcN+9mF8Dm+1wnoN93lvwOQXM1xnjb253UsVuguAjuoUYUamhnjez6VVA/TYqaGGp1SM0Y5GSYPI/9igEeppO9CR98qnV/yO5kmYI8eliP+W2y/nBq5tBLMZrofHT7gpTwM7INLUTZh2v6ADrW3D6AbQK6YGHRVXUDbl4ZHoEj8KUtJKtcdqmIGujoGBZS3+74wmzbjZu4cgGJP7lyme+ImC0T+0Zl6+vOzg0/dy7hWGMjtMw+QNRQHB30Pts=

Watch out, as JWT token and xsrf token are tied together, if you send a request to any path
without xsrf token, a new one will be created and you will likely have to re-login

For convenience, Postman requests were set up with these two as variables, so you can just 
change postman environment variables <b>jwt</b> and <b>xsrf</b> and have these tokens across 
all requests. Also, you can use these values, they are set up dor default <b>admin</b> user and 
they won't expire for 2 months:

xsrf: ab611fdb-ef2a-41de-ad80-ab2d5fddaf26
jwt: 9zLLFBxAe6lgUMiTHvMC370Ri3a0GQB9epYMZwUyEQy4iBZWvAzK5KGQvvAPoMLpIfO/UhcPIK+hwLWw62vHSWh046chBoWwlUh1ds4+PJTOrsSD3A2pf3hTC0WpWNSkSvXiP49G9GRtAKTnJTS/IAyVp5yXjRspcvRUh5Ze3IXlgBf+8AFs0rT0fN7XV9jebugHaKiz3jkLaiNRa5J9MSJSpk4CEgL76IrwqMn/KVjGfNeaQMfAaIKqKlklrPbIa+QzyjYoF81UD81s5/4gjyzHlkA8rC7cVZuIP8jO12VBp6q2pWNdOjaQB1Vu7ujrvpQe962lu9I=