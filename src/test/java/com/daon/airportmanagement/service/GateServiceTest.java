package com.daon.airportmanagement.service;

import com.daon.airportmanagement.exceptions.AirportmanagementException;
import com.daon.airportmanagement.repository.GateRepository;
import org.junit.jupiter.api.*;

import static org.mockito.Mockito.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GateServiceTest {

  GateService gateService;
  GateRepository mockGateRepository;
  ValidationService mockValidationService;

  @BeforeAll
  public void beforeAll() {
    mockGateRepository = mock(GateRepository.class);
    mockValidationService = mock(ValidationService.class);
    gateService = new GateService(mockGateRepository, mockValidationService);
  }

  @BeforeEach
  public void setUp() {

  }

  @Test
  public void createGateShouldThrowExcepitonIfGateExists() {
    when(mockGateRepository.existsByName(anyString())).thenReturn(true);
    try {
      gateService.createGate("A2", 1, 2);
    } catch (AirportmanagementException e) {
      return;
    }
    Assertions.fail("Gate should have thrown exception");
  }

}
