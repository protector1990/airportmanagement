package com.daon.airportmanagement.service;

import com.daon.airportmanagement.exceptions.AirportmanagementException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ValidationServiceTest {

  @Test
  public void validationServiceShouldNotAllowSingleNullToFromValue() {
    ValidationService validationService = new ValidationService();
    try {
      validationService.validateFromAndToHours(null, 5);
    } catch (AirportmanagementException ex) {
      return;
    }
    Assertions.fail("Validator should have thrown exception");
  }

  @Test
  public void validationServiceShouldNotAllowFromGreaterThanTo() {
    ValidationService validationService = new ValidationService();
    try {
      validationService.validateFromAndToHours(6, 5);
    } catch (AirportmanagementException ex) {
      return;
    }
    Assertions.fail("Validator should have thrown exception");
  }

  @Test
  public void validationServiceShouldNotAllowGreaterThanDayLength () {
    ValidationService validationService = new ValidationService();
    try {
      validationService.validateFromAndToHours(6, 86401);
    } catch (AirportmanagementException ex) {
      return;
    }
    Assertions.fail("Validator should have thrown exception");
  }
  
  @Test
  public void validationServiceShouldNotAllowNonExistngRole() {
    ValidationService validationService = new ValidationService();
    try {
      validationService.validateRole("DUMMY_ROLE");
    } catch (AirportmanagementException ex) {
      return;
    }
    Assertions.fail("Validator should have thrown exception");
  }
}
