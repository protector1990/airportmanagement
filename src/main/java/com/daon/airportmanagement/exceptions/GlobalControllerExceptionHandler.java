package com.daon.airportmanagement.exceptions;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.daon.airportmanagement.dto.ErrorDTO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {
  @ExceptionHandler(AirportmanagementException.class)
  public ResponseEntity<Object> handleAirportManagementException(AirportmanagementException ex, WebRequest request) {
    return handleExceptionInternal(ex, new ErrorDTO(ex.getErrorCode().getMessage()), new HttpHeaders(), HttpStatus.resolve(ex.getErrorCode().getHttpStatus()), request);
  }

  @ExceptionHandler({JWTVerificationException.class})
  public ResponseEntity<Object> handleAuthenticationException(Exception ex, WebRequest request) {
    return handleExceptionInternal(ex, new ErrorDTO("Unauthorized") , new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
  }

  @ExceptionHandler({AccessDeniedException.class})
  public ResponseEntity<Object> handleAccessDeniedException(Exception ex, WebRequest request) {
    return handleExceptionInternal(ex, new ErrorDTO("Forbidden") , new HttpHeaders(), HttpStatus.FORBIDDEN, request);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<Object> handleAllOtherExceptions(Exception ex, WebRequest request) {
    return handleExceptionInternal(ex, new ErrorDTO("Something went wrong") , new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
  }
}
