package com.daon.airportmanagement.exceptions;

import lombok.Getter;

@Getter
public enum ErrorCode {

  GATE_NOT_FOUND("Gate not found", 100, 404),
  GATE_ALREADY_EXISTS("Gate with that name already exists", 101, 400),
  GATE_HOURS_INVALID("Available hours for gate are invalid", 102, 400),
  NO_GATE_AVAILABLE("No gates available", 200, 409),
  FLIGHT_NOT_FOUND("Flight not found", 201, 404),
  ROLE_DOESNT_EXIST("Role does not exist", 300, 400),
  USERNAME_NOT_AVAILABLE("Username already taken", 300, 400);

  private String message;
  private Integer errorCode;
  private Integer httpStatus;

  ErrorCode(String message, Integer errorCode, Integer httpStatus) {
    this.message = message;
    this.errorCode = errorCode;
    this.httpStatus = httpStatus;
  }
}
