package com.daon.airportmanagement.exceptions;

import lombok.Getter;

@Getter
public class AirportmanagementException extends Exception {

  private ErrorCode errorCode;

  public AirportmanagementException(ErrorCode errorCode) {
    super(errorCode.getMessage());
    this.errorCode = errorCode;
  }
}
