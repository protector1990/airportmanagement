package com.daon.airportmanagement.dto;

import lombok.Data;

@Data
public class LoginDTO {
  private String username;
  private String password;
}
