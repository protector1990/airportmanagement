package com.daon.airportmanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GateDTO {

  private String gateId;

  private String flight;

  private Integer availableFrom;

  private Integer availableTo;
}
