package com.daon.airportmanagement.repository;

import com.daon.airportmanagement.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
  User findUserByUsername(String username);
  boolean existsByUsername(String username);
  Integer deleteByUsername(String username);
}
