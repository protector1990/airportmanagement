package com.daon.airportmanagement.repository;

import com.daon.airportmanagement.model.Gate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface GateRepository extends CrudRepository<Gate, Integer> {

  Optional<Gate> findGateByFlight(String flight);

  Optional<Gate> findGateByName(String name);

  Boolean existsByName(String name);

  List<Gate> findAllByFlightIsNotNull();

  @Query("select g from Gate g where g.flight = null and " +
          "((g.availableFrom < ?1 and g.availableTo > ?1) or g.availableFrom is null)")
  Gate findAvailableGate(int time);

}
