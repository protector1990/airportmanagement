package com.daon.airportmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@EnableRetry
@SpringBootApplication
public class AirportmanagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(AirportmanagementApplication.class, args);
	}

}
