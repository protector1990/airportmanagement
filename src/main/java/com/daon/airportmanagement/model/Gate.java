package com.daon.airportmanagement.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "gate")
public class Gate {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Integer id;

  @Column(name = "name")
  private String name;

  /**
   * Flight currently parked at the gate
   */
  @Column(name = "flight")
  private String flight;

  /**
   * Seconds since start of the day
   * @see com.daon.airportmanagement.service.ValidationService#validateFromAndToHours(Integer, Integer) 
   */
  @Column(name = "available_from")
  private Integer availableFrom;

  /**
   * Seconds since start of the day
   * @see com.daon.airportmanagement.service.ValidationService#validateFromAndToHours(Integer, Integer)
   */
  @Column(name = "available_to")
  private Integer availableTo;
}


