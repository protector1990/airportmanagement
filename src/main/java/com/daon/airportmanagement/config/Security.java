package com.daon.airportmanagement.config;

import com.daon.airportmanagement.repository.UserRepository;
import com.daon.airportmanagement.security.JWTAuthenticationFilter;
import com.daon.airportmanagement.security.JWTAuthorizationFilter;
import com.daon.airportmanagement.security.JWTCsrfTokenRepository;
import org.jasypt.util.text.BasicTextEncryptor;
import org.jasypt.util.text.TextEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class Security extends WebSecurityConfigurerAdapter {

  @Autowired
  UserRepository userRepository;

  @Autowired
  JWTCsrfTokenRepository jwtCsrfTokenRepository;

  @Bean
  public PasswordEncoder getPasswordEncoder() {
    return new BCryptPasswordEncoder(12);
  }

  @Bean
  public TextEncryptor getJWTTestEncryptor() {
    BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
    textEncryptor.setPasswordCharArray("jwt-encryption-secret".toCharArray()); // TODO: keep this in a secure place
    return textEncryptor;
  }

  @Autowired
  public void initialize(AuthenticationManagerBuilder builder, DataSource dataSource) throws Exception {
    builder.jdbcAuthentication()
            .dataSource(dataSource)
            .usersByUsernameQuery("select username as principal, password_hash as credentials, true from users where username = ?")
            .authoritiesByUsernameQuery("select username as principal, role from users where username = ?")
            .rolePrefix("ROLE_")
            .passwordEncoder(new BCryptPasswordEncoder());
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    CookieCsrfTokenRepository csrfTokenRepository = new CookieCsrfTokenRepository();
    csrfTokenRepository.setSecure(true);
    csrfTokenRepository.setCookieHttpOnly(true);
    http
      .csrf()
       .ignoringRequestMatchers(new AntPathRequestMatcher("/api/login", "POST"))
       .csrfTokenRepository(jwtCsrfTokenRepository);
    http
       .cors()
       .disable();
    http.authorizeRequests()
      .antMatchers(HttpMethod.POST, "/api/login").permitAll()
      .anyRequest().permitAll()
      .and()
      .addFilter(new JWTAuthenticationFilter(authenticationManager(), jwtCsrfTokenRepository, getJWTTestEncryptor()))
      .addFilter(new JWTAuthorizationFilter(authenticationManager(), userRepository, getJWTTestEncryptor()))
      .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }
}
