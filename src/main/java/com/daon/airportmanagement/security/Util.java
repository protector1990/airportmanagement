package com.daon.airportmanagement.security;

import org.apache.commons.lang3.StringUtils;
import org.jasypt.util.text.TextEncryptor;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import static com.daon.airportmanagement.security.Constants.JWT_COOKIE_NAME;


public class Util {

  public static String decryptJWTFromCookie(HttpServletRequest request, TextEncryptor jwtEncryptor) {
    Cookie cookie = WebUtils.getCookie(request, JWT_COOKIE_NAME);
    if (cookie == null) return null;
    String encryptedToken = cookie.getValue();
    if (StringUtils.isEmpty(encryptedToken)) return null;
    return jwtEncryptor.decrypt(encryptedToken);
  }
}
