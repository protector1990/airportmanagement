package com.daon.airportmanagement.security;

public enum Roles {
  ROLE_USER,
  ROLE_ADMIN
}
