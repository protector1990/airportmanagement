package com.daon.airportmanagement.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.daon.airportmanagement.dto.LoginDTO;
import com.daon.airportmanagement.dto.LoginResponseDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.jasypt.util.text.TextEncryptor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

import static com.daon.airportmanagement.security.Constants.*;

@Slf4j
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

  private final AuthenticationManager authenticationManager;
  private final ObjectMapper objectMapper;
  private final JWTCsrfTokenRepository jwtCsrfTokenRepository;
  private final TextEncryptor jwtEncryptor;


  public JWTAuthenticationFilter(
          AuthenticationManager authenticationManager,
          JWTCsrfTokenRepository jwtCsrfTokenRepository,
          TextEncryptor textEncryptor) {
    this.authenticationManager = authenticationManager;
    this.jwtCsrfTokenRepository = jwtCsrfTokenRepository;
    this.jwtEncryptor = textEncryptor;
    setFilterProcessesUrl("/api/login");
    objectMapper = new ObjectMapper();
  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse resp) throws AuthenticationException {
    LoginDTO loginInfo = null;
    try {
      loginInfo = objectMapper.readValue(req.getInputStream(), LoginDTO.class);
      return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
              loginInfo.getUsername(), loginInfo.getPassword()
      ));
    } catch (IOException e) {
      log.error("Could not read login data", e);
      return null;
    }
  }

  @Override
  protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
    super.unsuccessfulAuthentication(request, response, failed);
  }

  @Override
  protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
    Date expirationDate = new Date(System.currentTimeMillis() + JWT_TOKEN_EXPIRY);
    User user = (User)authResult.getPrincipal();
    String jwtToken  = JWT.create()
            .withSubject(user.getUsername())
            .withExpiresAt(expirationDate)
            .withClaim(CSRF_HEADER_NAME, jwtCsrfTokenRepository.csrfTokenHolder.getCsrfToken())
            .sign(Algorithm.HMAC512(JWT_SECRET.getBytes()));
    String encryptedJWTToken = jwtEncryptor.encrypt(jwtToken);
    writeJWTAsHeader(encryptedJWTToken, response);
    LoginResponseDTO loginResponseDTO = new LoginResponseDTO(expirationDate);
    response.getWriter().write(objectMapper.writeValueAsString(loginResponseDTO));
    response.getWriter().flush();
  }

  private void writeJWTAsHeader(String token, HttpServletResponse response) {
    Cookie cookie = new Cookie(JWT_COOKIE_NAME, token);
    cookie.setSecure(true);
    cookie.setHttpOnly(true);
    cookie.setPath("/");
    response.addCookie(cookie);
  }
}
