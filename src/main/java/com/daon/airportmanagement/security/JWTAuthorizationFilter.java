package com.daon.airportmanagement.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.daon.airportmanagement.model.User;
import com.daon.airportmanagement.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jasypt.util.text.TextEncryptor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.daon.airportmanagement.security.Constants.*;

@Slf4j
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

  private final UserRepository userRepository;
  private final TextEncryptor jwtEncryptor;

  public JWTAuthorizationFilter(AuthenticationManager authenticationManager,
                                UserRepository userRepository,
                                TextEncryptor encryptor) {
    super(authenticationManager);
    this.userRepository = userRepository;
    this.jwtEncryptor = encryptor;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
    String decryptedJWTToken = Util.decryptJWTFromCookie(request, jwtEncryptor);
    if (!StringUtils.isEmpty(decryptedJWTToken)) {
      UsernamePasswordAuthenticationToken authentication = getAuthenticationFromJWT(decryptedJWTToken);
      SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    chain.doFilter(request, response);
  }

  private UsernamePasswordAuthenticationToken getAuthenticationFromJWT(String token) {
    String user = JWT.require(Algorithm.HMAC512(JWT_SECRET.getBytes()))
            .build()
            .verify(token)
            .getSubject();

    if (user != null) {
      User appUser = userRepository.findUserByUsername(user);
      if (appUser == null) {
        log.error("User not found");
        return null;
      }
      return new UsernamePasswordAuthenticationToken(user, null, getAuthorities(appUser));
    }

    return null;
  }

  /**
   * Gets user authorities. Since roles are ment to be permission levels, ADMIN role will also have USER Authority
   * added
   * @param user
   * @return
   */
  private List<GrantedAuthority> getAuthorities(User user) {
    String role = user.getRole();
    List<GrantedAuthority> authorities = new ArrayList<>();
    List<String> roles = Arrays.asList(Roles.values()).stream().map(r -> r.toString()).collect(Collectors.toList());
    if (!StringUtils.isEmpty(role) && roles.contains(role)) {
      for (String currRole : roles) {
        authorities.add(() -> currRole);
        if (role.equals(currRole)) {
          break;
        }
      }
    }
    return authorities;
  }
}
