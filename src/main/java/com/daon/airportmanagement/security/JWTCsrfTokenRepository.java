package com.daon.airportmanagement.security;

import java.util.UUID;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth0.jwt.JWT;
import org.jasypt.util.text.TextEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import static com.daon.airportmanagement.security.Constants.*;

@Component
public final class JWTCsrfTokenRepository implements CsrfTokenRepository {

  @Autowired
  CsrfTokenHolder csrfTokenHolder;

  @Autowired
  TextEncryptor jwtEncryptor;

  public JWTCsrfTokenRepository() {
  }

  @Override
  public CsrfToken generateToken(HttpServletRequest request) {
    return new DefaultCsrfToken(CSRF_HEADER_NAME, CSRF_PARAMETER_NAME, createNewToken());
  }

  @Override
  public void saveToken(CsrfToken token, HttpServletRequest request, HttpServletResponse response) {
    String tokenValue = token != null ? token.getToken() : "";
    csrfTokenHolder.setCsrfToken(tokenValue); // to be incorporated into jwt as a claim by Authentication filter
    Cookie cookie = new Cookie(CSRF_COOKIE_NAME, tokenValue);
    cookie.setSecure(true);
    cookie.setMaxAge(token != null ? -1 : 0);
    cookie.setHttpOnly(false);
    cookie.setPath("/");

    response.addCookie(cookie);
  }

  @Override
  public CsrfToken loadToken(HttpServletRequest request) {
    String decryptedJWTToken = Util.decryptJWTFromCookie(request, jwtEncryptor);
    if (decryptedJWTToken != null) {
      String csfrValue = JWT.decode(decryptedJWTToken).getClaim(CSRF_HEADER_NAME).asString();
      return !StringUtils.hasLength(csfrValue) ? null : new DefaultCsrfToken(CSRF_HEADER_NAME, CSRF_PARAMETER_NAME, csfrValue);
    }
    return null;
  }

  private String getRequestContext(HttpServletRequest request) {
    String contextPath = request.getContextPath();
    return contextPath.length() > 0 ? contextPath : "/";
  }

  private String createNewToken() {
    return UUID.randomUUID().toString();
  }
}
