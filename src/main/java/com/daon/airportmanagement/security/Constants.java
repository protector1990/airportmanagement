package com.daon.airportmanagement.security;

public class Constants {
  static final long JWT_TOKEN_EXPIRY = 5200000L;
  static final String JWT_SECRET = "Keep this in a more secure place";
  static final String JWT_COOKIE_NAME = "X-JWT-COOKIE";

  static final String CSRF_PARAMETER_NAME = "_csrf";
  static final String CSRF_HEADER_NAME = "X-XSRF-TOKEN";
  static final String CSRF_COOKIE_NAME = "XSRF-TOKEN";
}
