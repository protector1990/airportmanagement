package com.daon.airportmanagement.security;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import static org.springframework.web.context.WebApplicationContext.SCOPE_REQUEST;

@Component
@Scope(scopeName = SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CsrfTokenHolder {
  private String csrfToken;
  public String getCsrfToken() {
    return csrfToken;
  }
  public void setCsrfToken(String token) {
    csrfToken = token;
  }
}
