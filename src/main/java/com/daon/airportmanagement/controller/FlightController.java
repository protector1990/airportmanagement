package com.daon.airportmanagement.controller;

import com.daon.airportmanagement.dto.FlightResponseDTO;
import com.daon.airportmanagement.exceptions.AirportmanagementException;
import com.daon.airportmanagement.model.Gate;
import com.daon.airportmanagement.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/flight")
public class FlightController {

  @Autowired
  FlightService flightService;

  @GetMapping("/all")
  public List<FlightResponseDTO> getAllFlights() {
    List<Gate> gates = flightService.getAllFlights();
    return gates
            .stream()
            .map(g -> new FlightResponseDTO(g.getFlight(), g.getName()))
            .collect(Collectors.toList());
  }

  @GetMapping("/{flightId}")
  public Gate getFlightsGate(@PathVariable("flightId") String flightId) throws AirportmanagementException {
    return flightService.getFlightsGate(flightId);
  }

  @PutMapping("/{flightId}")
  public Gate requestGate(@PathVariable("flightId") String flightId) throws Exception {
    return flightService.parkFlight(flightId);
  }

  @DeleteMapping("/{flightId}")
  public Gate departFlight(@PathVariable("flightId") String flightId) throws Exception {
    return flightService.departFlight(flightId);
  }

}
