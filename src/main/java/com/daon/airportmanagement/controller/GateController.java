package com.daon.airportmanagement.controller;

import com.daon.airportmanagement.dto.GateDTO;
import com.daon.airportmanagement.exceptions.AirportmanagementException;
import com.daon.airportmanagement.model.Gate;
import com.daon.airportmanagement.service.GateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/gate")
public class GateController {

  @Autowired
  private GateService gateService;

  @GetMapping("/{gateName}")
  public Gate getGate(@PathVariable("gateName") String gateName) throws AirportmanagementException {
    return gateService.getGate(gateName);
  }

  @GetMapping("/all")
  public Iterable<Gate> getAllGates()  {
    return gateService.getAllGates();
  }

  @PostMapping("/{gateName}")
  public Gate createGate(@PathVariable("gateName") String gateName, @RequestBody GateDTO gateDTO) throws AirportmanagementException {
    return gateService.createGate(gateName, gateDTO.getAvailableFrom(), gateDTO.getAvailableTo());
  }

  @PutMapping("/{gateId}/setHours")
  public Gate setGateHours(@PathVariable("gateId") String gateId, @RequestBody GateDTO gateDTO) throws AirportmanagementException {
    return gateService.updateGateHours(gateId, gateDTO.getAvailableFrom(), gateDTO.getAvailableTo());
  }

  @PutMapping("/{gateId}/markAsAvailable")
  public Gate markGateAsAvailable(@PathVariable("gateId") String gateId) throws AirportmanagementException {
    return gateService.markGateAsAvailable(gateId);
  }
}
