package com.daon.airportmanagement.controller;

import com.daon.airportmanagement.dto.UserDTO;
import com.daon.airportmanagement.exceptions.AirportmanagementException;
import com.daon.airportmanagement.model.User;
import com.daon.airportmanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserController {

  @Autowired
  UserService userService;

  @PostMapping()
  public User createUser(@RequestBody UserDTO userDTO) throws AirportmanagementException {
    return userService.createUser(userDTO.getUsername(), userDTO.getPassword(), userDTO.getRole());
  }
}
