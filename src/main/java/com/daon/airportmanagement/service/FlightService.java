package com.daon.airportmanagement.service;

import com.daon.airportmanagement.annotation.DataRetry;
import com.daon.airportmanagement.exceptions.AirportmanagementException;
import com.daon.airportmanagement.model.Gate;
import com.daon.airportmanagement.repository.GateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalTime;
import java.util.List;

import static com.daon.airportmanagement.exceptions.ErrorCode.FLIGHT_NOT_FOUND;
import static com.daon.airportmanagement.exceptions.ErrorCode.NO_GATE_AVAILABLE;

@Service
public class FlightService {

  @Autowired
  private GateRepository gateRepository;

  @Secured("ROLE_USER")
  @DataRetry
  @Transactional
  public Gate parkFlight(String flight) throws AirportmanagementException {
    Gate gate = gateRepository.findGateByFlight(flight).orElse(null);
    if (gate != null) {
      return gate;
    }
    else {
      Gate newGate = gateRepository.findAvailableGate(LocalTime.now().toSecondOfDay());
      if (newGate == null) {
        // No available gates
        throw new AirportmanagementException(NO_GATE_AVAILABLE);
      }
      newGate.setFlight(flight);
      return newGate;
    }
  }

  @Secured("ROLE_USER")
  @DataRetry
  @Transactional
  public Gate departFlight(String flight) throws AirportmanagementException {
    Gate gate = gateRepository.findGateByFlight(flight).orElse(null);
    if (gate != null) {
      gate.setFlight(null);
      return gate;
    } else {
      // Flight is not on the airport
      throw new AirportmanagementException(FLIGHT_NOT_FOUND);
    }
  }

  @DataRetry
  @Transactional
  public Gate getFlightsGate(String flight) throws AirportmanagementException {
    return gateRepository.findGateByFlight(flight).orElseThrow(() -> new AirportmanagementException(FLIGHT_NOT_FOUND));
  }

  @DataRetry
  @Transactional
  public List<Gate> getAllFlights() {
    return gateRepository.findAllByFlightIsNotNull();
  }

}
