package com.daon.airportmanagement.service;

import com.daon.airportmanagement.annotation.DataRetry;
import com.daon.airportmanagement.exceptions.AirportmanagementException;
import com.daon.airportmanagement.exceptions.ErrorCode;
import com.daon.airportmanagement.model.User;
import com.daon.airportmanagement.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static com.daon.airportmanagement.exceptions.ErrorCode.USERNAME_NOT_AVAILABLE;

@Service
public class UserService {

  @Autowired
  UserRepository userRepository;

  @Autowired
  PasswordEncoder passwordEncoder;

  @Secured("ROLE_ADMIN")
  @DataRetry
  @Transactional(rollbackOn = Exception.class)
  public User createUser(String username, String password, String role) throws AirportmanagementException {
    if (userRepository.existsByUsername(username)){
      throw new AirportmanagementException(USERNAME_NOT_AVAILABLE);
    }
    if (role.equals("ROLE_ADMIN")) {
      deleteDefaultAdmin();
    }
    String passwordHash = passwordEncoder.encode(password);
    return userRepository.save(new User(null, username, passwordHash, role));
  }

  private void deleteDefaultAdmin() {
    if (userRepository.existsByUsername("admin")) {
      userRepository.deleteByUsername("admin");
    }
  }

}
