package com.daon.airportmanagement.service;

import com.daon.airportmanagement.annotation.DataRetry;
import com.daon.airportmanagement.exceptions.AirportmanagementException;
import com.daon.airportmanagement.model.Gate;
import com.daon.airportmanagement.repository.GateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static com.daon.airportmanagement.exceptions.ErrorCode.GATE_ALREADY_EXISTS;
import static com.daon.airportmanagement.exceptions.ErrorCode.GATE_NOT_FOUND;

@Service
public class GateService {

  private GateRepository gateRepository;

  private ValidationService validationService;

  @Autowired
  public GateService(GateRepository gateRepository, ValidationService validationService) {
    this.gateRepository = gateRepository;
    this.validationService = validationService;
  }

  @DataRetry
  @Transactional
  public Iterable<Gate> getAllGates() {
    return gateRepository.findAll();
  }

  @DataRetry
  @Transactional
  public Gate getGate(String name) throws AirportmanagementException {
    return gateRepository.findGateByName(name).orElseThrow(() -> new AirportmanagementException(GATE_NOT_FOUND));
  }

  @Secured("ROLE_ADMIN")
  @DataRetry
  @Transactional
  public Gate createGate(String gateName, Integer from, Integer to) throws AirportmanagementException {
    if (gateRepository.existsByName(gateName)) {
      throw new AirportmanagementException(GATE_ALREADY_EXISTS);
    }
    validationService.validateFromAndToHours(from, to);
    return gateRepository.save(new Gate(null, gateName, null, from, to));
  }

  // from and to are seconds since the start of the day
  @Secured("ROLE_ADMIN")
  @DataRetry
  @Transactional
  public Gate updateGateHours(String gateName, Integer from, Integer to) throws AirportmanagementException {
    Gate gate = gateRepository.findGateByName(gateName).orElseThrow(() -> new AirportmanagementException(GATE_NOT_FOUND));
    validationService.validateFromAndToHours(from, to);
    gate.setAvailableFrom(from);
    gate.setAvailableTo(to);
    return gateRepository.save(gate);
  }
  
  // marks gate as with having no flights pakred. If outside available hours, it will still be unavailable
  @Secured("ROLE_USER")
  @DataRetry
  @Transactional
  public Gate markGateAsAvailable(String gateName) throws AirportmanagementException {
    Gate gate = gateRepository.findGateByName(gateName).orElseThrow(() -> new AirportmanagementException(GATE_NOT_FOUND));
    gate.setFlight(null);
    return gateRepository.save(gate);
  }

}
