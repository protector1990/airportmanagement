package com.daon.airportmanagement.service;

import com.daon.airportmanagement.exceptions.AirportmanagementException;
import com.daon.airportmanagement.model.Gate;
import com.daon.airportmanagement.security.Roles;
import org.springframework.stereotype.Service;

import java.util.Arrays;

import static com.daon.airportmanagement.exceptions.ErrorCode.GATE_HOURS_INVALID;
import static com.daon.airportmanagement.exceptions.ErrorCode.ROLE_DOESNT_EXIST;

@Service
public class ValidationService {

  private static final int SECONDS_PER_DAY = 86400;

  /**
   * Must both non-null or both null, to must be a greater value than from, and neither should exceed number of seconds
   * within a 24hour period
   * @param from
   * @param to
   * @throws AirportmanagementException
   * @see Gate#getAvailableFrom()
   */
  public void validateFromAndToHours(Integer from, Integer to) throws AirportmanagementException {
    if ((from == null) ^ (to == null)) {
      throw new AirportmanagementException(GATE_HOURS_INVALID);
    }
    if (from != null) {
      if (to > SECONDS_PER_DAY) {
        throw new AirportmanagementException(GATE_HOURS_INVALID);
      }
      if (from >= to) {
        throw new AirportmanagementException(GATE_HOURS_INVALID);
      }
    }
  }

  /**
   * Validate that a string represents a valid role in our parmission system
   * @param role
   * @throws AirportmanagementException if role doesn't exist
   */
  public void validateRole(String role) throws AirportmanagementException {
    if (Arrays.asList(Roles.values()).stream().map(r -> r.name()).noneMatch(r -> r.equals(role))) {
      throw new AirportmanagementException(ROLE_DOESNT_EXIST);
    }
  }
}
