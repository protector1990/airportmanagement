package com.daon.airportmanagement.annotation;

import org.springframework.dao.NonTransientDataAccessException;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.dao.TransientDataAccessException;
import org.springframework.retry.annotation.Retryable;
import org.springframework.transaction.TransactionException;

@Retryable(
        include = {
                TransactionException.class,
                TransientDataAccessException.class,
                RecoverableDataAccessException.class
        },
        exclude = {
                NonTransientDataAccessException.class
        }
)
public @interface DataRetry {
}
