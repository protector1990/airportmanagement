create table users (
    id serial primary key,
    username varchar(30),
    password_hash varchar (100),
    role varchar(15),
    unique (username)
);

create table gate (
    id serial primary key,
    name varchar(10),
    flight varchar(10),
    available_from integer,
    available_to integer,
    unique(flight),
    unique(name)
);

create index gate_available_from_index on gate(available_from);
create index gate_available_to_index on gate(available_to);

-- default admin. Gets deleted when the first admin user is created
insert into users (username, password_hash, role) values ('admin', '$2a$12$WBgi9w4CP9yupxWRQU5TrOe.VOzyDJ6TQbPBbaUwxzAsxS8OGh20.', 'ROLE_ADMIN');